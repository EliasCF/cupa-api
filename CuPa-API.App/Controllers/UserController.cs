using System.Linq;
using CuPa_API.App.Core;
using CuPa_API.App.Models;
using CuPa_API.App.Models.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CuPa_API.App.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private UserRepository _repository { get; }

        public UserController(UserRepository repository) 
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetUsers([FromQuery]PaginationFilter pagination) 
        {
            IQueryable<User> users = _repository.Get();
            return Ok(users.ToList());
        }
    }
}