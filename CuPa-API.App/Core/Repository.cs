using System.Linq;
using CuPa_API.App.Data;
using Microsoft.EntityFrameworkCore;

namespace CuPa_API.App.Core
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected DbSet<TEntity> _db { get; }

        public Repository(ApplicationDbContext context) 
        {
            _db = context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get() 
        {
            return _db.AsQueryable();
        }
    }
}