using CuPa_API.App.Data;
using CuPa_API.App.Models;

namespace CuPa_API.App.Core
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(ApplicationDbContext context) : base(context) { }
    }
}