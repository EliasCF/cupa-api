using CuPa_API.App.Data;
using CuPa_API.App.Models;

namespace CuPa_API.App.Core
{
    public class PostRepository : Repository<Post>
    {
        public PostRepository(ApplicationDbContext context) : base(context) { }
    }
}