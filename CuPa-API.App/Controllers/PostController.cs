using System.Linq;
using CuPa_API.App.Core;
using CuPa_API.App.Models;
using CuPa_API.App.Models.Pagination;
using Microsoft.AspNetCore.Mvc;

namespace CuPa_API.App.Controllers
{
    [ApiController]
    [Route("posts")]
    public class PostController : ControllerBase
    {
        private PostRepository _repository { get; }

        public PostController(PostRepository repository) 
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetPosts([FromQuery]PaginationFilter pagination) 
        {
            IQueryable<Post> posts = _repository.Get();
            return Ok(posts.ToList());
        }
    }
}