using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using CuPa_API.App.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace CuPa_API.App.Data.Extensions
{
    public static class IApplicationBuilderExtensions
    {
        public static async Task GenerateSeedData(this IApplicationBuilder app) 
        {
            using IServiceScope scope = app.ApplicationServices.CreateScope();
            using ApplicationDbContext context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            if (context.Users.Any()) return;

            List<User> users = new Faker<User>()
                .RuleFor(f => f.FirstName, f => f.Name.FirstName())
                .RuleFor(f => f.LastName, f => f.Name.LastName())
                .RuleFor(f => f.Email, f => f.Internet.Email())
                .RuleFor(f => f.CreatedDate, f => f.Date.Between(DateTime.Now.AddYears(-5), DateTime.Now))
                .Generate(100000);

            await context.Users.AddRangeAsync(users);
            await context.SaveChangesAsync();
        }
    }
}