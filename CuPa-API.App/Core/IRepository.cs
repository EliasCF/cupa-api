using System.Linq;

namespace CuPa_API.App.Core
{
    public interface IRepository<TEntity> 
        where TEntity : class
    {
        IQueryable<TEntity> Get();
    }
}