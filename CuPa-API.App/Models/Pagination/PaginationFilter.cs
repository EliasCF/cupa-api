namespace CuPa_API.App.Models.Pagination
{
    public class PaginationFilter
    {
        public string Before { get; set; }

        public string After { get; set; }
        
        public int Limit { get; set; } = 5;
    }
}